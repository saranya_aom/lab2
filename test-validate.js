const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', () => {
    context('Function isUserNameValid', () => {
        it('Function prototype : boolean isUserNameValid(username: String)', () => {
            expect(validate.isUserNameValid('kob')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', () => {
            expect(validate.isUserNameValid('tu')).to.be.false;
        });
        it('ทุกตัวต้องเป็นตัวเล็ก', () => {
            expect(validate.isUserNameValid('Kob')).to.be.false;
            expect(validate.isUserNameValid('koB')).to.be.false;
        });
        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', () => {
            expect(validate.isUserNameValid('kob123456789012')).to.be.true;
            expect(validate.isUserNameValid('kob1234567890123')).to.be.false;
        });
    });
    context('Function isAgeValid', () => {
        it('Function prototype : boolean isAgeValid (age: String)', () => {
            expect(validate.isAgeValid('18')).to.be.true;
        });
        it('age ต้องเป็นข้อความที่เป็นตัวเลข', () => {
            expect(validate.isAgeValid('a')).to.be.false;
        });
        it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', () => {
            expect(validate.isAgeValid('17')).to.be.false;
            expect(validate.isAgeValid('18')).to.be.true;
            expect(validate.isAgeValid('100')).to.be.true;
            expect(validate.isAgeValid('101')).to.be.false;
        });
    });
    context('Function isPasswordValid', () => {
        it('Function prototype : boolean isPasswordValid(password: String)', () => {
            expect(validate.isPasswordValid('Password#123')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', () => {
            expect(validate.isPasswordValid('Abcd#123')).to.be.true;
            expect(validate.isPasswordValid('Abcd#1234')).to.be.true;
            expect(validate.isPasswordValid('abcdefg')).to.be.false;

        });
        it('ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', () => {
            expect(validate.isPasswordValid('Aom#60160051')).to.be.true;
            expect(validate.isPasswordValid('teacherkob#1')).to.be.false;

        });
        it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', () => {
            expect(validate.isPasswordValid('Aom#60160051')).to.be.true;
            expect(validate.isPasswordValid('Aommmmm#11')).to.be.false;
        });
        it('ต้องมีอักขระพิเศษอย่างน้อย 1 ตัว', () => {
            expect(validate.isPasswordValid('Aom-60160051')).to.be.true;
            expect(validate.isPasswordValid('aom#60160051')).to.be.false;
            expect(validate.isPasswordValid('Aom60160051')).to.be.false;

        });
    });
    context('Function isDateValid', () => {
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)', () => {
            expect(validate.isDateValid('01', '01', '2020')).to.be.true;
        });
        it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', () => {
            expect(validate.isDateValid('01', '01', '2020')).to.be.true;
            expect(validate.isDateValid('31', '01', '2020')).to.be.true;
            expect(validate.isDateValid('02', '01', '2020')).to.be.true;
            expect(validate.isDateValid('00', '01', '2020')).to.be.false;
            expect(validate.isDateValid('32', '01', '2020')).to.be.false;
        });
        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน', () => {
            expect(validate.isDateValid('01', '01', '2020')).to.be.true;
            expect(validate.isDateValid('01', '12', '2020')).to.be.true;
            expect(validate.isDateValid('01', '06', '2020')).to.be.true;
            expect(validate.isDateValid('01', '00', '2020')).to.be.false;
            expect(validate.isDateValid('01', '13', '2020')).to.be.false;
        });

        it('เดือนแต่ละเดือนมีจำนวนวันต่างกัน', () => {
            expect(validate.isDateValid('31', '01', '2020')).to.be.true;
            expect(validate.isDateValid('28', '02', '2020')).to.be.true;
            expect(validate.isDateValid('31', '03', '2020')).to.be.true;
            expect(validate.isDateValid('30', '04', '2020')).to.be.true;
            expect(validate.isDateValid('31', '05', '2020')).to.be.true;
            expect(validate.isDateValid('30', '06', '2020')).to.be.true;
            expect(validate.isDateValid('31', '07', '2020')).to.be.true;
            expect(validate.isDateValid('31', '08', '2020')).to.be.true;
            expect(validate.isDateValid('30', '09', '2020')).to.be.true;
            expect(validate.isDateValid('31', '10', '2020')).to.be.true;
            expect(validate.isDateValid('30', '11', '2020')).to.be.true;
            expect(validate.isDateValid('31', '12', '2020')).to.be.true;

            expect(validate.isDateValid('32', '01', '2020')).to.be.false;
            expect(validate.isDateValid('30', '02', '2000')).to.be.false;
            expect(validate.isDateValid('29', '02', '1999')).to.be.false;
            expect(validate.isDateValid('32', '03', '2020')).to.be.false;
            expect(validate.isDateValid('31', '04', '2020')).to.be.false;
            expect(validate.isDateValid('32', '05', '2020')).to.be.false;
            expect(validate.isDateValid('31', '06', '2020')).to.be.false;
            expect(validate.isDateValid('32', '07', '2020')).to.be.false;
            expect(validate.isDateValid('32', '08', '2020')).to.be.false;
            expect(validate.isDateValid('31', '09', '2020')).to.be.false;
            expect(validate.isDateValid('32', '10', '2020')).to.be.false;
            expect(validate.isDateValid('31', '11', '2020')).to.be.false;
            expect(validate.isDateValid('32', '12', '2020')).to.be.false;
        });
    });
});