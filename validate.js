module.exports = {
    isUserNameValid: function(username) {
        if (username.length < 3 || username.length > 15) {
            return false;
        }
        if (username.toLowerCase() !== username) {
            return false;
        }
        if (username.length < 3) {
            return false;
        }
        return true;
    },
    isAgeValid: function(age) {
        if (isNaN(parseInt(age))) {
            return false;
        }
        if (parseInt(age) < 18 || parseInt(age) > 100) {
            return false;
        }
        return true;
    },
    isPasswordValid: function(password) {
        if (password.length < 8) return false;
        for (i = 0, num = 0, ch = 0, sym = 0; i < password.length; i++) {
            if (password.charAt(i) == password.charAt(i).match(/[A-Z]/) || !isNaN(password.charAt(i)) || password.charAt(i).match(/[\!\@\#\$\%\^\&\*\(\)\_\+\|\~\\\=\`\{\}\:\"\;\'\<\>\?\,\.\/\-]/)) {
                if (password.charAt(i) == password.charAt(i).match(/[A-Z]/)) ch++;
                else if (!isNaN(password.charAt(i))) num++;
                else if (password.charAt(i).match(/[\!\@\#\$\%\^\&\*\(\)\_\+\|\~\=\`\{\}\:\"\;\'\<\>\?\,\.\/\-]/)) sym++;
            }
            if (num >= 3 && ch >= 1 && sym >= 1) break;
        }
        if (password == password.toLowerCase() || num < 3 || sym < 1 || ch < 1)
            return false;
        return true;
    },
    isDateValid: function(day, month, year) {
        if ((day < 1) || (month < 1 || month > 12) || (year < 1970 || year > 2020))
            return false;
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            if (day > 31)
                return false;
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            if (day > 30)
                return false;
        } else if (month == 2) {
            if (year % 100 == 0 && year % 400 == 0 && day > 29)
                return false;
            else if (day > 28)
                return false;
        }
        return true;
    }
}